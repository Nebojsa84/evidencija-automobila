﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EvidencijaAutomobila.Models
{
    public class Car
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        [StringLength(50, MinimumLength =2)]
        public string Manufacturer { get; set; }

        [Required(ErrorMessage ="This field is required!")]
        [StringLength(50, MinimumLength = 2,ErrorMessage ="Must be longer then 2 and les then 50 characters")]
        public string Model { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        [StringLength(50, MinimumLength = 2)]
        public string Year { get; set; }

        [Required(ErrorMessage = "This field is required!")]
        [StringLength(4, MinimumLength = 2,ErrorMessage = "Must be longer then 2 and less then 4 characters")]
        public string Volume { get; set; }

        public Engine Engine { get; set; }
        
    }
}