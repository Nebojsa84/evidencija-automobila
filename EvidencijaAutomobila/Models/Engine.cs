﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EvidencijaAutomobila.Models
{
    public class Engine
    {
        public int Id { get; set; }

       // [Display( Name ="Engine name")]
       // [Required(ErrorMessage = "This field is required!")]
       // [StringLength(50, MinimumLength = 2)]
        public string EngineName { get; set; }
    }
}