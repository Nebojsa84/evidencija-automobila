﻿using EvidencijaAutomobila.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EvidencijaAutomobila.ViewModel
{
    public class CarEngineViewModel
    {
        public IEnumerable<Engine> Engines { get; set; }
        public Car Car { get; set; }
        public int selected { get; set; }
    }
}