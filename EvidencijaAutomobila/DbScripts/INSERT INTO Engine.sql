INSERT INTO Engine(EngineName)
VALUES 
('3.0L TFSI Supercharged DOHC V-6 '),
('3.0L N55 Turbocharged DOHC I-6'),
('1.6L Turbocharged DOHC I-4'),
('3.6L Pentastar DOHC V-6'),
('5.0L DOHC V-8'),
('1.4L DOHC I-4/111kW Drive Motor'),
('5.0L Tau DOHC V-8'),
('80kW AC Synchronous Electric Motor'),
('2.0L DOHC I-4 Turbodiesel'),
('3.0L Turbocharged DOHC I-6');
