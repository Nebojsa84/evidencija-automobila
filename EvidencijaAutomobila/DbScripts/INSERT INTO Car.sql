INSERT INTO Car(Manufacturer,Model,Year,Volume,EngineId)
VALUES
('Audi','Audi S4','1994','1.6',1),
('BMW','BMW 335i','2006','1.3',2),
('BMW ','Mini Cooper S','2009','1.3',3),
('Dodge','Dodge Avenger','1994','1.8',4),
('Ford','Ford Mustang GT','2001','1.8',5),
('Chevrolet','Chevrolet Volt','2012',null,6),
('Hyundai','Hyundai Genesis','2011','1.6',7),
('Nissan','Nissan Leaf','2009',null,8),
('Volkswagen','Volkswagen Jetta TDI','2000','1.3',9),
('Volvo','Volvo S60','2006','1.8',10);
