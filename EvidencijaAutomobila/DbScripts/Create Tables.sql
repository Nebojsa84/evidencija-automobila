
CREATE TABLE Engine(
EngineId int primary key identity (1,1),
EngineName nvarchar(50)
);

CREATE TABLE Car(
CarId int primary key identity (1,1),
Manufacturer nvarchar(50),
Model nvarchar(50),
Year nvarchar(5),
Volume nvarchar(10),
EngineId int,
FOREIGN KEY (EngineId) REFERENCES Engine(EngineId),
);  
