﻿using EvidencijaAutomobila.Models;
using EvidencijaAutomobila.Repository;
using EvidencijaAutomobila.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EvidencijaAutomobila.Controllers
{
    public class EngineController : Controller
    {
        IEngineRepository EngineRepository = new EngineRepository();

        // GET: Engine
        public ActionResult Index()
        {
            return View(EngineRepository.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Engine en)
        {
            if (ModelState.IsValid)
            {
                
                EngineRepository.Create(en);
                return RedirectToAction("Index");
            }
            else
            {
                return View(en);
            }
            
        }

        public ActionResult Delete(int id)
        {
            EngineRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}