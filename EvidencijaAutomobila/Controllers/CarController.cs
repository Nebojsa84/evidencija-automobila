﻿using EvidencijaAutomobila.Models;
using EvidencijaAutomobila.Repository;
using EvidencijaAutomobila.Repository.Interfaces;
using EvidencijaAutomobila.ViewModel;
using System.Linq;
using System.Web.Mvc;

namespace EvidencijaAutomobila.Controllers
{
    public class CarController : Controller
    {
        ICarRepository CarRepository = new CarRepository();
        IEngineRepository EngineRepository = new EngineRepository();

        // GET: Car
        public ActionResult Index()
        {
            return View(CarRepository.GetAll().ToList());
        }

        public ActionResult Create()
        {
            CarEngineViewModel cewm = new CarEngineViewModel();
            cewm.Car = null;
            cewm.Engines = EngineRepository.GetAll();

            return View(cewm);

        }
        [HttpPost]
        public ActionResult Create(Car car)
        {
            if (ModelState.IsValid)
            {
                              
                CarRepository.Create(car);
                return RedirectToAction("Index");
            }
            CarEngineViewModel cevm = new CarEngineViewModel();
            cevm.Car = car;
            cevm.Engines = EngineRepository.GetAll();
            return View(cevm);

        }

        public ActionResult Update(int id)
        {
            CarEngineViewModel vm = new CarEngineViewModel();
            Car c = CarRepository.GetById(id);

            vm.Car = c;
            vm.Engines = EngineRepository.GetAll();
            vm.selected = c.Engine.Id;
            return View(vm);
        }

        [HttpPost]
        public ActionResult Update(CarEngineViewModel c)
        {
            if (ModelState.IsValid)
            {
                CarRepository.Update(c.Car);
                return RedirectToAction("Index");
            }
            else
            {
                c.Engines = EngineRepository.GetAll();
                return View(c);
            }

        }

        public ActionResult Delete(int id)
        {
            CarRepository.Delete(id);
            return RedirectToAction("Index");
        }
    }
}