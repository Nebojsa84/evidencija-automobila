﻿using EvidencijaAutomobila.Models;
using EvidencijaAutomobila.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidencijaAutomobila.Repository.Interfaces
{
    interface ICarRepository
    {
        IEnumerable<Car> GetAll();
        Car GetById(int id);
        bool Create(Car c);
        void Delete(int id);
        void Update(Car c);
    }
}
