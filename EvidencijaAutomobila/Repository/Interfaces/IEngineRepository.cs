﻿using EvidencijaAutomobila.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EvidencijaAutomobila.Repository.Interfaces
{
    interface IEngineRepository
    {
        IEnumerable<Engine> GetAll();
        bool Create(Engine en);
        void Delete(int id);

    }
}
