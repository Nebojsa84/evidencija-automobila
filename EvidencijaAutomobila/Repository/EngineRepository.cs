﻿using EvidencijaAutomobila.Models;
using EvidencijaAutomobila.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EvidencijaAutomobila.Repository
{
    public class EngineRepository : IEngineRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connString = ConfigurationManager.ConnectionStrings["CarDbContext"].ToString();
            conn = new SqlConnection(connString);
        }
        public IEnumerable<Engine> GetAll()
        {
            string query = "SELECT * FROM Engine ";
            List<Engine> Engines = new List<Engine>();
            DataTable dt = new DataTable();
            Connection();
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            foreach (DataRow dr in dt.Rows)
            {               
                Engine en = new Engine();
                en.Id = (int)dr["EngineId"];
                en.EngineName = dr["EngineName"].ToString();
                
                Engines.Add(en);
            }

            return (Engines);
        }

        public bool Create(Engine en)
        {
            string query = "INSERT INTO Engine(EngineName) VALUES (@EngineName)";
            Connection();
            using(SqlCommand cmd=new SqlCommand(query, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@EngineName",en.EngineName);
                if ((cmd.ExecuteNonQuery()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Delete(int id)
        {
            string query = "DELETE FROM Engine WHERE EngineId=@EngineId";
            Connection();
            using(SqlCommand cmd=new SqlCommand(query, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("EngineId", id);
                cmd.ExecuteNonQuery();
            }
        }
    }
}