﻿using EvidencijaAutomobila.Models;
using EvidencijaAutomobila.Repository.Interfaces;
using EvidencijaAutomobila.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EvidencijaAutomobila.Repository
{   
    public class CarRepository : ICarRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connString = ConfigurationManager.ConnectionStrings["CarDbContext"].ToString();
            conn = new SqlConnection(connString);
        }

        public bool Create(Car c)
        {
            string query = "INSERT INTO Car(Manufacturer,Model,Year,Volume,EngineId) " +
                                       "Values(@Manufacturer,@Model,@Year,@Volume,@EngineId)";

            Connection();
            using(SqlCommand cmd =new SqlCommand(query,conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Manufacturer",c.Manufacturer);
                cmd.Parameters.AddWithValue("@Model", c.Model);
                cmd.Parameters.AddWithValue("@Year", c.Year);
                cmd.Parameters.AddWithValue("@Volume", c.Volume);
                cmd.Parameters.AddWithValue("@EngineId", c.Engine.Id);
                if ((cmd.ExecuteNonQuery()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public void Delete(int id)
        {
            string query = "DELETE FROM Car WHERE CarId=@CarId";
            Connection();
            using (SqlCommand cmd = new SqlCommand(query,conn))
            {

                conn.Open();
                cmd.Parameters.AddWithValue("@CarId",id);
                cmd.ExecuteNonQuery();

            }
        }

        public IEnumerable<Car> GetAll()
        {
            string query = "SELECT * FROM Car c LEFT JOIN Engine e ON c.EngineID=e.EngineId";
            List<Car> Cars = new List<Car>();
            DataTable dt = new DataTable();
            Connection();
            using (SqlCommand cmd=new SqlCommand(query,conn))
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            foreach(DataRow dr in dt.Rows)
            {
                Car car = new Car();
                car.Id = (int)dr["CarId"];
                car.Manufacturer = dr["Manufacturer"].ToString();
                car.Model = dr["Model"].ToString();
                car.Year = dr["Year"].ToString();
                car.Volume =dr["Volume"].ToString();
                Engine en = new Engine();
                en.Id =(int)dr["EngineId"];
                en.EngineName = dr["EngineName"].ToString();
                car.Engine = en;
                Cars.Add(car);
            }

            return (Cars);
        }

        public Car GetById(int id)
        {
            string query = "SELECT * FROM Car c LEFT JOIN Engine e ON c.EngineID=e.EngineId WHERE CarId=@CarId";
            List<Car> Cars = new List<Car>();
            DataTable dt = new DataTable();
            Connection();
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@CarId", id);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
            }

            DataRow row = dt.Rows[0];
                                  
            Car car = new Car();
            car.Id = (int)row["CarId"];
            car.Manufacturer = row["Manufacturer"].ToString();
            car.Model = row["Model"].ToString();
            car.Year = row["Year"].ToString();
            car.Volume = row["Volume"].ToString();
            Engine en = new Engine();
            en.Id = (int)row["EngineId"];
            en.EngineName = row["EngineName"].ToString();
            car.Engine = en;
                           
            return (car);
        }

        public void Update(Car c)
        {
            string query = "UPDATE Car SET Manufacturer=@Manufacturer,Model=@Model,Year=@Year,Volume=@Volume,EngineId=@EngineId WHERE CarId=@CarId";

            Connection();
            using (SqlCommand cmd = new SqlCommand(query, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@CarId", c.Id);
                cmd.Parameters.AddWithValue("@Manufacturer", c.Manufacturer);
                cmd.Parameters.AddWithValue("@Model", c.Model);
                cmd.Parameters.AddWithValue("@Year", c.Year);
                cmd.Parameters.AddWithValue("@Volume", c.Volume);
                cmd.Parameters.AddWithValue("@EngineId", c.Engine.Id);
                cmd.ExecuteNonQuery();
               
            }
        }
    }
}